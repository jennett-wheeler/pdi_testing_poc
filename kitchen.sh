#!/bin/sh

if ! [[ -f ${PDI_HOME:?Error! Location of Pentaho DI client installation not found - use env var PDI_HOME}/kitchen.sh ]]; then
    echo "Error! Could not find 'kitchen.sh' in the supplied Pentaho DI client installation location: PDI_HOME=${PDI_HOME}" >&2
    exit 1
fi

export REPO_DIR=$(readlink -f $(dirname ${BASH_SOURCE}))
export KETTLE_REPOSITORY=repo

orig_kettle_home="${KETTLE_HOME:-${HOME}}"
export KETTLE_HOME=$(mktemp -d)
echo "KITCHEN WRAPPER SCRIPT: Setting up temporary KETTLE_HOME: ${KETTLE_HOME}"
mkdir -p ${KETTLE_HOME}/.kettle

function cleanup {
    retval=$?
    
    echo "KITCHEN WRAPPER SCRIPT: Exited at $(date +"%Y-%m-%d %H:%M:%S") with exit code: ${retval}"
    
    # Remove temp KETTLE_HOME directory
    if [[ -d ${KETTLE_HOME} ]]; then
        echo "KITCHEN WRAPPER SCRIPT: Removing temporary KETTLE_HOME: ${KETTLE_HOME}"
        rm -r ${KETTLE_HOME}
    fi
    
    exit $retval
}
trap "cleanup" INT TERM EXIT

echo "KITCHEN WRAPPER SCRIPT: Generating kettle.properties from environment variables"
printenv > ${KETTLE_HOME}/.kettle/kettle.properties

if [[ -f ${orig_kettle_home}/.kettle/kettle.properties ]]; then
	echo "KITCHEN WRAPPER SCRIPT: Adding user's kettle.properties to the bottom of the generated kettle.properties"
	cat ${orig_kettle_home}/.kettle/kettle.properties >> ${KETTLE_HOME}/.kettle/kettle.properties
fi

echo "KITCHEN WRAPPER SCRIPT: Generating repositories.xml to point to this repo: REPO_DIR=${REPO_DIR}"
cat > ${KETTLE_HOME}/.kettle/repositories.xml << END_FILE
<?xml version="1.0" encoding="UTF-8"?>
<repositories>
    <repository>
        <id>KettleFileRepository</id>
        <name>${KETTLE_REPOSITORY}</name>
        <description>di-repo</description>
        <base_directory>${REPO_DIR}</base_directory>
        <read_only>N</read_only>
        <hides_hidden_files>N</hides_hidden_files>
    </repository>
</repositories>
END_FILE

echo "KITCHEN WRAPPER SCRIPT: Running: ${PDI_HOME}/kitchen.sh $@"
${PDI_HOME}/kitchen.sh "$@"
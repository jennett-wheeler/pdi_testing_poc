# PDI File Base Repo
To use this template, fork or copy this repository and clone to your PC.

## GUI Client
1. Setup your PDI Spoon Client with a File Based Repository that points to this repo.
1. Develop as usual (you can use your personal kettle.properties for this).

## Running On Server
To run code (using kitchen) use the included kitchen.sh script.

It will generate a kettle.properties, so any variables required by your PDI jobs should be exported in your wrapping scripes or your profile scripts.

You need to point to the server's PDI client installation directory using the variable ${PDI_HOME}

This script also exports the location of the script to the ${REPO_DIR} variable, but you should design your repo's to not be aware of their location.

## From existing EE repo
To convert an existing repo to a file based repo, you need to first export the EE repo.

Once you have then set up your fork / copy of this file based repo with Spoon, you can import your jobs in the same way that you would import from Pre-Prod to Prod.

You may have to reset up your connections, but doing so should create kdb files in the root.

### Notes about old EE way of working
When using EE repos, it was common to create a directory per user with a copy of the code they were working on.

However, for file based repos, you can use a good git branching strategy, being careful to avoid working on the same file at once (merging these sorts of issue can be more difficult)

Make sure you regularly push and pull commits and communicate with the team if you change code that may be used by others (e.g. common jobs / transformations).

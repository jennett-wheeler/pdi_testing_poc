#!/bin/bash
FILE=${1?Error missing file parameter}
if ! [ -f ${FILE} ]; then
    echo "Error file could not be found."
    exit 1
fi
if ! [[ $FILE == *".ktr" ]]; then
    echo "Error file doesn't end in '.ktr'."
    exit 1
fi

xslt="<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>

    <xsl:output method='xml' encoding='UTF-8' />

    <xsl:template match='/transformation'>
        <xsl:copy>
            <xsl:apply-templates select=\"*[not(contains('+step+order+notepads+',concat('+',local-name(),'+')))]\" />
            <xsl:apply-templates select='step'>
                <xsl:sort select='GUI/xloc/text()' data-type='number' order='ascending' />
                <xsl:sort select='GUI/yloc/text()' data-type='number' order='ascending' />
            </xsl:apply-templates>
            <order>
                <xsl:apply-templates select='order/hop'>
                    <xsl:sort select='from/text()' order='ascending' />
                    <xsl:sort select='to/text()' order='ascending' />
                </xsl:apply-templates>
            </order>
            <notepads>
                <xsl:apply-templates select='notepads/notepad'>
                    <xsl:sort select='xloc/text()' data-type='number' order='ascending' />
                    <xsl:sort select='yloc/text()' data-type='number' order='ascending' />
                </xsl:apply-templates>
            </notepads>
        </xsl:copy>
    </xsl:template>

    <xsl:template match='@*|node()'>
        <xsl:copy>
            <xsl:apply-templates select='@*|node()' />
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>"

echo "${xslt}" | xsltproc -o "${FILE}.tmp" - "${FILE}" &&
xmllint --format "${FILE}.tmp" > "${FILE}" &&
sed -i "s/[&][#]13[;]//g" "${FILE}" &&
rm "${FILE}.tmp" &&
echo "Reformatted ${FILE}"
